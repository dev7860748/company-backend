# Company Backend

This project was generated with NodeJS version 16.14.0

## Database

You can find the database file in the project root `company_db.sql`.

## Development server

After importing the db to your sql server, run `npm start` for this service to start. 
After that, you can start sending requests to port 3000 (`http://localhost:3000/`)