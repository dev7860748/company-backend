const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');

const app = new Koa();

const router = new Router();

const pool = require('./db');

app.use(cors());

router.get('/client', async (ctx) => {
  const { q: searchTerm, status: statusParam, birth_date: birthDateParam } = ctx.query;
  const queryParams = [];

  try {
    let sql = 'SELECT * FROM client WHERE 1';

    if (searchTerm) {
      sql += ' AND (CONCAT(first_name, " ", last_name) LIKE ? OR email LIKE ?)';
      const searchPattern = `%${searchTerm}%`;
      queryParams.push(searchPattern, searchPattern);
    }

    if (statusParam) {
      const statuses = statusParam.split(',').map(status => status.trim());
      sql += ` AND status IN (${statuses.map(() => '?').join(', ')})`;
      queryParams.push(...statuses);
    }

    if (birthDateParam) {
      sql += ' AND birth_date = ?';
      queryParams.push(birthDateParam);
    }

    const [rows] = await pool.query(sql, queryParams);
    ctx.body = rows;
  } catch (err) {
    console.error('Error fetching/searching clients:', err);
    ctx.status = 500;
    ctx.body = 'Error fetching/searching clients';
  }
});





router.post('/client', async (ctx) => {
  const { first_name, last_name, email, birth_date, ip_address, status } = ctx.request.body;

  try {
    const sql = 'INSERT INTO client (first_name, last_name, email, birth_date, registration_date, ip_address, status) VALUES (?, ?, ?, ?, ?, ?, ?)';
    await pool.query(sql, [first_name, last_name, email, birth_date, Date.now(), ip_address, status]);
    ctx.status = 201;
    ctx.body = { message: 'Client added successfully' };
  } catch (err) {
    console.error('Error adding client:', err);
    ctx.status = 500;
    ctx.body = 'Error adding client';
  }
});


app.use(bodyParser());
app.use(router.routes());
app.use(router.allowedMethods());

const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
